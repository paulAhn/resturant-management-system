from django.db import models

# Create your models here.

#식당엔티티
class Restaurant(models.Model):
    class meta:
        db_table= '식당'
    restaurant_name= models.CharField(max_length=50, null=True, blank=True, verbose_name='사명') #식당명
    license_number= models.CharField(max_length=30, null=True, blank=True, verbose_name='사업자번호') #사업자번호
    business= models.CharField(max_length=50, null=True, blank=True, verbose_name='업종') #업종
    restaurant_pic= models.ImageField('식당사진', null=True, blank=True, default="") #식당사진
    registration_date= models.DateField(null=True, blank=True, verbose_name='등록일') # 등록일
    restaurant_address= models.CharField(max_length=100, null=True, blank=True, verbose_name='주소') #주소

    def __str__(self):
        return self.restaurant_name

#사용자엔티티
class User(models.Model):
    class meta:
        db_table = '사용자'
    restaurant= models.ForeignKey(Restaurant, on_delete=models.CASCADE, blank=True, null=True, verbose_name='소속명') #식당외래키
    name= models.CharField(max_length=10, null=True, blank=True, verbose_name='이름') #사용자명
    user_id= models.CharField(max_length=30, default="", verbose_name='ID') #사용자id
    user_pw= models.CharField(max_length=30, default="", verbose_name='PW') #사용자pw
    phone= models.CharField(max_length=50, null=True,blank=True, default="없음", verbose_name='핸드폰') #사용자 핸드폰
    e_mail= models.CharField(max_length=50, null=True,blank=True, default="없음", verbose_name='이메일') #사용자 이메일
    user_pic= models.ImageField('사진', null=True, blank=True, default="") #사용자 사진
    user_bank= models.CharField(max_length=20, null=True, blank=True, verbose_name='은행명') #사용자은행
    user_banknumber= models.CharField(max_length=30, null=True, blank=True, verbose_name='계좌번호')

    def __str__(self):
        return self.name
    
#종업원엔티티
class Employee(models.Model):
    class meta:
        db_table= '종업원'
    restaurant= models.ForeignKey(Restaurant, on_delete=models.CASCADE, blank=True, null=True, verbose_name='소속명')
    employee_name= models.CharField(max_length=10, null=True, blank=True, verbose_name='직원명')
    joining_date= models.DateField(null=True, blank=True, verbose_name='입사일')
    retire_date= models.DateField(null=True, blank=True, verbose_name='퇴사일')
    employee_pic= models.ImageField('종업원사진', null=True, blank=True, default="")
    sex = models.CharField(max_length=10, null=True, blank=True, verbose_name='성별')
    employee_phone= models.CharField(max_length=50, null=True,blank=True, default="없음", verbose_name='핸드폰') 
    employee_email= models.CharField(max_length=50, null=True,blank=True, default="없음", verbose_name='이메일')
    resident_number = models.CharField(max_length=20, null=True, blank=True, verbose_name='주민등록번호')
    employee_address= models.CharField(max_length=100, null=True, blank=True, verbose_name='주소')
    employee_bank= models.CharField(max_length=20, null=True, blank=True, verbose_name='은행명')
    employee_banknumber= models.CharField(max_length=30, null=True, blank=True, verbose_name='계좌번호')

    def __str__(self):
        return self.employee_name